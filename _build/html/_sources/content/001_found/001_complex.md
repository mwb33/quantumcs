# Complex Numbers

## Quadratic Formula

A **quadratic formula** has the form

$
\begin{align}
    f(x) =& ax^2 + bx + c
\end{align}
$

For some contants $a,b,c$ and variable $x$.

We can set the constants to make a specific instance of the general quadratic formula.

$
\begin{align}
    f(x) =& 5x^2 + 2x - 3
\end{align}
$

This function creates a parabola. We may want to know the **roots** of the quadratic formula. These are the points at which $f(x)=0$. We can plot the function and the line $g(x)=0$ to see where they intersect.

![Roots of a Quadratic Function](plots/quad_01.png)

This function intersects with the zero line at two points. This means there are two roots of the function.

In our case, the roots are $x=-1$ and $x=\frac{3}{5}$. If we plug either of these numbers in, the function evaluates to zero.

The first root is -1.

$
\begin{align}
    f(-1) =& 5(-1)^2 + 2(-1) - 3 \\
        =& 5 -2 - 3 \\
        =& 5-5 \\
        =& 0
\end{align}
$

The second root is $\frac{3}{5}$.

$
\begin{align}
    f\left(\frac{3}{5}\right) =& 5\left(\frac{3}{5}\right)^2 + 2\left(\frac{3}{5}\right) - 3 \\
        =& 5\left( \frac{9}{25} \right) + \frac{6}{5} - 3 \\
        =& \frac{9}{5} + \frac{6}{5} - 3 \\
        =& \frac{15}{5} - 3 \\
        =& 3 - 3 \\
        =& 0
\end{align}
$

The roots of every quadratic expression can be solved using the following formula.

$
\begin{align}
    x =& \frac{ -b \pm \sqrt{b^2-4ac}}{2a}
\end{align}
$

We can plug in the values from our example. We have $a=5$, $b=2$, and $c=-3$.

$
\begin{align}
    x =& \frac{ -2 \pm \sqrt{2^2-4 * 5 * (-3)}}{2*5} \\
    =& \frac{-2 \pm \sqrt{64}}{10} \\
    =& \frac{-2 \pm 8}{10}
\end{align}
$

We use use the plus version we get $x=\frac{6}{10}=\frac{3}{5}$. If we use the minus version then we will get $x=\frac{-10}{10}=-1$. The equation gives us both zeros.

This expression does work for every possible quadratic expression. We can plug in the expression for $x$ and verify the answer is zero.

$
\begin{align}
	0 =& ax^2+bx+c \\ 
	=& a\left(\frac{-b \pm \sqrt{b^2-4ac}}{2a}\right)^2+b\left(\frac{-b \pm \sqrt{b^2-4ac}}{2a}\right)+c \\
	=& \left(
		\frac{b^2 \pm -b\sqrt{b^2-4ac}-2ac }{2a}
	\right)+\left(\frac{-b^2 \pm b\sqrt{b^2-4ac}}{2a}\right)+c \\
	=&\frac{(b^2-b^2)\pm(-b\sqrt{b^2-4ac}+b\sqrt{b^2-4ac})-2ac}{2a}+c \\
	=&c-\frac{2ac}{2a} \\
	=&0
\end{align}
$

What happens if we try different quadratic expressions?

$
\begin{align}
    h(x) = x^2 + 2x + 3
\end{align}
$

When we plot this function, we see that it never touches the zero line.

![Imaginary Answers](plots/quad_02.png)

It appears that this function has no answers. We know $a=1$, $b=2$, and $c=2$. What happens if we try to compute the answers?

$
\begin{align}
x=&\frac{-2 \pm \sqrt{2^2-4*1*3}}{2*1} \\
    =&\frac{-2 \pm \sqrt{-8}}{2}
\end{align}
$

What is the square root of $-8$? We know that $8=2*2*2$ To get negative eight, we need one of the following to happen:

$
\begin{align}
    -8 =& (-2) * 2 * 2 \\
    -8 =& (-2) * (-2) * (-2)
\end{align}
$

We can simplify $\frac{-2 \pm \sqrt{-8}}{2}$ to $-1 \pm \sqrt{-2}$. This as far as we can get without knowing what $\sqrt{-1}$ is. We don't know of any number that can be squared to get $-1$. Regardless, we also have this $\sqrt{-1}$ appear in our problem.

We introduce a new symbol, the imaginary number $i=\sqrt{-1}$.

We know have our two solutions $x=-1 \pm i \sqrt{2}$. The equation has two answers, but they are imaginary. They do not appear on the plot because we cannot plot $\sqrt{-1}$ on this simple cartesian plane.

## Imaginary Numbers

The **Real Numbers** are the set of numbers that do not include $i$. If we combine all **Real** and **Imaginary** numbers, we get the set of **Complex Numbers**.

What happens when we compute the exponents of $i$?

$
\begin{align}
    i^0 =& 1 \\
    i^1 =& \sqrt{-1} \\
    i^2 =& -1 \\
    i^3 =& -i \\
    i^4 =& 1
\end{align}
$

A **Complex Number** is any number that has for format $a+bi$ where $a$ and $b$ are real numbers. 

We can work with **Complex Numbers**. 

We can add two complex numbers together.

$
\begin{align}
    (a+bi) + (x+yi) =& (a+x) + (b+y) i
\end{align}
$

We can subtract two complex numbers.

$
\begin{align}
    (a+bi) - (x+yi) =& (a-x) + (b-y)i
\end{align}
$

We can multiply two complex numbers.

$
\begin{align}
(a+bi) * (x+yi) =& (ax-by) + (ay+bx)i
\end{align}
$

Using the multiplication formula we can also square a complex number. This can be extended for any exponent.

$
\begin{align}
(a+bi)^2 =& (a^2-b^2)+2abi
\end{align}
$

Next, we introduce some additional operations on **Complex Numbers**.

The **Absolute Value** of a complex number is given by the following expression. The absolute value is the distance from the origin (0,0) to the point (a,b) on the cartesian plane.

$
\begin{align}
		\left|(a+bi)\right| = \sqrt{a^2+b^2}
\end{align}
$

We can find the **reciprocal** of a complex number.

$
\begin{align}
	\frac{1}{a+bi} = \frac{a}{a^2+b^2}+\frac{-b}{a^2+b^2}i
\end{align}
$

We can also find the **complex conjugate**.

$
\begin{align}
	(a+bi)^{*} = a-bi
\end{align}
$

What happens when we multiply a number by it's **complex conjugate**?

$
\begin{align}
	(a+bi)(a+bi)^{*} 
	=& (a+bi)(a-bi) \\
	=& a^2-abi+abi+b^2 \\
	=& a^2+b^2 \\
	=& \left| a+bi \right|^2
\end{align}
$

The **complex conjugate** is a mirror of the imaginary number across the x-axis. Some areas us $\overline{a+bi}$ as the **complex conjugate** symbol. This book will use the $(a+bi)^{*}$ notation. 

<a title="Oleg Alexandrov, CC BY-SA 3.0 &lt;http://creativecommons.org/licenses/by-sa/3.0/&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Complex_conjugate_picture.svg"><img width="256" alt="Complex conjugate picture" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/Complex_conjugate_picture.svg/256px-Complex_conjugate_picture.svg.png"></a>

## Euler's Number

Euler's Number, also called the natural number, is the solution to the following summation.

$
\begin{align}
    e =& \sum_{n=0}^{\infty} \left( \frac{1}{n!} \right) \\
        =& 1 + \frac{1}{1} + \frac{1}{2} + \frac{1}{6} + \cdots \\
        \approx& 2.71828
\end{align}
$

Euler's number appears in many areas. One of the most common is the computation of compound interest.

Euler's number also has a geometric definition. It is the point $p$ at which the area under the curve $\frac{1}{x}$ from $1$ to $p$ is equal to exactly 1.

<a title="Cronholm144 at English Wikipedia, CC BY-SA 3.0 &lt;http://creativecommons.org/licenses/by-sa/3.0/&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Hyperbola_E.svg"><img width="512" alt="Hyperbola E" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Hyperbola_E.svg/512px-Hyperbola_E.svg.png"></a>

We can write this using an integeral. We want to know $p$ such that

$
\begin{align}
    \int_{1}^{p} \frac{1}{x} = 1
\end{align}
$

The answer to this question is $p=e$.

It is also the base of the natural log.

$
\begin{align}
    \ln e^k = k
\end{align}
$

It also appears in Euler's identity which is ludicrous and shouldn't be true.

$
\begin{align}
    e^{i \pi} + 1 = 0
\end{align}
$

There is a relationship between $e$ and Complex Numbers.

$
\begin{align}
    e^{ix} =& \cos x + i \sin x
\end{align}
$

The formula for $e^{ix}$ creates a circle! We can take a point $a+bi$ and convert it to a value $re^{ix}$ to get a point on the circle.

<a title="Original:  GuntherDerivative work:  Wereon, CC BY-SA 3.0 &lt;http://creativecommons.org/licenses/by-sa/3.0/&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Euler%27s_formula.svg"><img width="512" alt="Euler&#039;s formula" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Euler%27s_formula.svg/512px-Euler%27s_formula.svg.png"></a>

Every Complex Number can be represented by a point on the circle. Let $z=a+bi$ be a complex number, then we can fine a matching radius $r$ and angle $\theta$ on the circle.

$
\begin{align}
	r=& \left| z \right| \\
	\theta =& \tan^{-1}(b/a)
\end{align}
$

We can figure out where on the circle the complex number $9+5i$ is.

$
\begin{align}
	r = & |9+5i| = \sqrt{9^2+5^2} = 56.62155 \\
	\theta =& \tan^{-1} \frac{5}{9} = 0.50798 \text{ Radians}
\end{align}
$

![Location of 9+5i](plots/polar.png)

Representing complex numbers as points on a circle gives us a geometric realization of the numbers. This can provide advantages when working with complex numbers. For example, the **complex conjugate** is easily visualized.

The **complex conjugate** of $9+5i$ is $9-5i$. The points on the circle are computed as

$
\begin{align}
	r = & |9-5i| = \sqrt{9^2+(-5)^2} = 56.62155 \\
	\theta =& \tan^{-1} \frac{-5}{9} = -0.50798 \text{ Radians}
\end{align}
$

![Location of 9+5i and Conjugate](plots/polar2.png)

## C++ Class

As a running example, we will simulate the mathematics behind each example using C++. The first component we need is a **Complex Number** class. There is one included in the *standard library* but for educational purpose we will build up our own.
