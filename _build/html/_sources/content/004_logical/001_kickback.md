# Phase Kickback

```{math}
\newcommand{\qzero}[0]{\left|0\right\rangle}
\newcommand{\qone}[0]{\left|1\right\rangle}
\newcommand{\qplus}[0]{\left| + \right\rangle}
\newcommand{\qminus}[0]{\left| - \right\rangle}
\newcommand{\dirac}[1]{\left| #1 \right\rangle}
\newcommand{\ident}[0]{\mathrm{I}}
```

Phase kickback is a critical feature of quantum computers. When a set of qubits are entangled, there is a relationship between them. When controlled actions are completed, the control bit may also be changed. Up until this point, the CX gate has appeared to be a conditional statement. This was only true because we only tested with certain kinds of inputs.

## Phase Kickback Example

We will return to the CX Gate.

$
\begin{align}
    CX=&
    \begin{bmatrix} 
        1 & 0 & 0 & 0 \\
        0 & 1 & 0 & 0 \\
        0 & 0 & 0 & 1 \\
        0 & 0 & 1 & 0
    \end{bmatrix}
\end{align}
$

We can apply a Hadamard gate to the first input of the CX gate.

![CX with an H Gate](kickback/kickback_01.png)

We want to see what happens in the circuit. We will start with a zero state.

$
\begin{align}
    S=& \begin{bmatrix}
            1 \\
            0 \\
            0 \\
            0 \\
        \end{bmatrix}
\end{align}
$

The first application is the H gate on wire $q_{0}$ and the Identity on $q_{1}$. We can use the tensor product generate the matrix.

$
\begin{align}
    H \oplus I =&
        \frac{1}{\sqrt{2}}
        \begin{bmatrix}
            1 & 1 \\
            1 & -1 
        \end{bmatrix}
        \oplus
        \begin{bmatrix}
            1 & 0 \\
            0 & 1 
        \end{bmatrix}
    \\
    =& 
    \frac{1}{\sqrt{2}}
    \begin{bmatrix}
        1 & 0 & 1 & 0 \\ 
        0 & 1 & 0 & 1 \\
        1 & 0 & -1 & 0 \\
        0 & 1 & 0 & -1
    \end{bmatrix}
\end{align}
$

We can apply this to the state vector. The result is what happens after the H gate is applied.

$
\begin{align}
(H \oplus I) * S =&
\frac{1}{\sqrt{2}}
    \begin{bmatrix}
        1 & 0 & 1 & 0 \\
        0 & 1 & 0 & 1 \\
        1 & 0 & -1 & 0 \\
        0 & 1 & 0 & -1
    \end{bmatrix}
    \begin{bmatrix}
            1 \\
            0 \\
            0 \\
            0 \\
    \end{bmatrix}
\\
=&
\frac{1}{\sqrt{2}}
\begin{bmatrix}
    1+0+0+0 \\
    0+0+0+0 \\
    1+0+0+0 \\
    0+0+0+0
\end{bmatrix}
=&
\frac{1}{\sqrt{2}}
\begin{bmatrix}
    1 \text{ (q0=0, q1=0)} \\
    0 \text{ (q0=0, q1=1)} \\
    1 \text{ (q0=1, q1=0)} \\
    0 \text{ (q0=1, q1=1)}
\end{bmatrix}
\end{align}
$

The system is in a split between the two states of $q_0$ but $q_1$ is always zero. 

The Bloch sphere at this point is shown below.

![Bloch Sphere after H](kickback/sphere_01.png)

The next step is to apply the CX gate. We expect the state (q0=0, q1=0) to be unchanged. We also expect the values of (q0=1, q1=0) and (q0=1, q1=1) to swap.

$
\begin{align}
CX * S =& 
	\begin{bmatrix} 
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 0 & 1 \\
		0 & 0 & 1 & 0
	\end{bmatrix}
    \left(
        \frac{1}{\sqrt{2}}
    \begin{bmatrix}
        1\\
        0\\
        1\\
        0
    \end{bmatrix}
    \right)
    \\
    =&
    \frac{1}{\sqrt{2}}
    \begin{bmatrix}
    1+0+0+0 \\
    0+0+0+0 \\
    0+0+0+0 \\
    0+0+1+0
    \end{bmatrix}
    \\
    =&
    \frac{1}{\sqrt{2}}
    \begin{bmatrix}
    1 \text{ (q0=0, q1=0)} \\
    0 \text{ (q0=0, q1=1)} \\
    0 \text{ (q0=1, q1=0)} \\
    1 \text{ (q0=1, q1=1)}
    \end{bmatrix}
\end{align}
$

No suprises here. The control qubit causes the result qubit to flip. The Bloch Spheres are shown below. In this case, we cannot really visualize the possibilities without drawing two different sets of spheres. The first pair is $(q0=0, q1=0)$ and the second pair is $(q0=1, q1=1)$.

![Spheres after CX applied when q0=0](kickback/sphere_02a.png)

![Spheres after CX applied when q0=1](kickback/sphere_02b.png)

We can see both possible cases represented on the Q-Sphere vizualization.

![Q-Sphere with Both Options](kickback/qsphere_02.png)

What happens if we add an H gate to the second wire, $q_1$? We should get four possible outcomes.

![Two H Gates](kickback/kickback_02.png)

This time we start with two H gates.

$
\begin{align}
(H \oplus H) =& 
\left(
\frac{1}{\sqrt{2}}
        \begin{bmatrix}
            1 & 1 \\
            1 & -1 
        \end{bmatrix}
\right)
\oplus
\left(
\frac{1}{\sqrt{2}}
        \begin{bmatrix}
            1 & 1 \\
            1 & -1 
        \end{bmatrix}
\right)
\\
=& \frac{1}{2}
\begin{bmatrix}
    1 & 1 & 1 & 1 \\
    1 & -1 & 1 & -1 \\
    1 & 1 & -1 & -1 \\
    1 & -1 & -1 & 1 
\end{bmatrix}
\end{align}
$

What happens when we apply this to the initial state.

$
\begin{align}
(H \oplus H)* S =&
\frac{1}{2}
\begin{bmatrix}
    1 & 1 & 1 & 1 \\
    1 & -1 & 1 & -1 \\
    1 & 1 & -1 & -1 \\
    1 & -1 & -1 & 1 
\end{bmatrix}
    \begin{bmatrix}
            1 \\
            0 \\
            0 \\
            0 \\
    \end{bmatrix}
\\
=& \frac{1}{2}
\begin{bmatrix}
    1 \text{ (q0=0, q1=0)} \\
    1 \text{ (q0=0, q1=1)} \\
    1 \text{ (q0=1, q1=0)} \\
    1 \text{ (q0=1, q1=1)} 
\end{bmatrix}
\end{align}
$

We got an even split between all three outcomes.

Next, we apply the CX Gate.

$
\begin{align}
CX * S =& 
	\begin{bmatrix} 
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 0 & 1 \\
		0 & 0 & 1 & 0
	\end{bmatrix}
    \left(
\frac{1}{2}
    \begin{bmatrix}
    1 \\
    1 \\
    1 \\
    1
    \end{bmatrix}
    \right)
    \\
    =&
    \frac{1}{2}
    \begin{bmatrix}
    1 \text{ (q0=0, q1=0)} \\
    1 \text{ (q0=0, q1=1)} \\
    1 \text{ (q0=1, q1=0)} \\
    1 \text{ (q0=1, q1=1)} 
    \end{bmatrix}
\end{align}
$

There is a $\frac{1}{2}$ in each position. The probability of all outcomes is equal.

What happens if we put an X on the $q_0$ wire? Your initial guess should be *nothing*. We would set the qubit to 1, but then the H gate would put it into a superposition between 0 and 1. That is not exactly true. This will show a quantum property in addition to our classical logical thinking.

![X Gate Added](kickback/kickback_03.png)

The key difference is in the first step. A barrier is used it illustrate that the remaining parts of the circuit are unchanged. The first matrix is $(I \oplus X)$.

$
\begin{align}
(I \oplus X) =& 
    \begin{bmatrix}
        1 & 0 \\
        0 & 1 
    \end{bmatrix}
    \oplus
    \begin{bmatrix}
        0 & 1 \\
        1 & 0 
    \end{bmatrix}
    \\
    =&
    \begin{bmatrix}
    0 & 1 & 0 & 0 \\
    1 & 0 & 0 & 0 \\
    0 & 0 & 0 & 1 \\
    0 & 0 & 1 & 0
    \end{bmatrix}
\end{align}
$

We start the circuit again with this as the initial gate.

$
\begin{align}
    (I \oplus X)*S =&
    \begin{bmatrix}
    0 & 1 & 0 & 0 \\
    1 & 0 & 0 & 0 \\
    0 & 0 & 0 & 1 \\
    0 & 0 & 1 & 0
    \end{bmatrix}
    \begin{bmatrix}
            1 \\
            0 \\
            0 \\
            0 \\
    \end{bmatrix}
    \\
    =&
    \begin{bmatrix}
    0+0+0+0 \\
    1+0+0+0 \\
    0+0+0+0 \\
    0+0+0+0 
    \end{bmatrix}
    \\
    =&
    \begin{bmatrix}
    0 \text{ (q0=0, q1=0)} \\
    1 \text{ (q0=0, q1=1)} \\
    0 \text{ (q0=1, q1=0)} \\
    0 \text{ (q0=1, q1=1)} 
    \end{bmatrix}
\end{align}
$

Everything seems reasonable here. We have set $q_1$ to be 1. Next we apply the $H \oplus H$ matrix we already built.

$
\begin{align}
(H \oplus H)* S =&
\frac{1}{2}
\begin{bmatrix}
    1 & 1 & 1 & 1 \\
    1 & -1 & 1 & -1 \\
    1 & 1 & -1 & -1 \\
    1 & -1 & -1 & 1 
\end{bmatrix}
    \begin{bmatrix}
            0 \\
            1 \\
            0 \\
            0 \\
    \end{bmatrix}
\\
=& \frac{1}{2}
\begin{bmatrix}
    1 \text{ (q0=0, q1=0)} \\
    -1 \text{ (q0=0, q1=1)} \\
    1 \text{ (q0=1, q1=0)} \\
    -1 \text{ (q0=1, q1=1)}
\end{bmatrix}
\end{align}
$

There is one **huge** difference this time. Notice that two of the values have negative signs applied. This will be important in the next stage.

We can look at the Bloch Sphere for the two qubits.

![The X gate flips the phase.](kickback/sphere_03.png)

Qubit $q_1$ is facing the opposite direction. It is still along the equator of the sphere, but it is pointing in the other direction.

We apply the CX gate next. This time, we get a slightly different result.

$
\begin{align}
CX * S =&
\begin{bmatrix} 
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 0 & 1 \\
		0 & 0 & 1 & 0
	\end{bmatrix}
\left(
\frac{1}{2}
\begin{bmatrix}
    1\\
    -1\\
    1 \\
    -1 
\end{bmatrix}
\right)
\\
=&
\frac{1}{2}
\begin{bmatrix}
    1 \text{ (q0=0, q1=0)} \\
    -1 \text{ (q0=0, q1=1)} \\
    -1 \text{ (q0=1, q1=0)} \\
    1 \text{ (q0=1, q1=1)} 
\end{bmatrix}
\end{align}
$

If we think about the *probabilities* of getting results, they are unchanged.

$
\begin{align}
\lvert 1/2  \rvert ^2 =& \frac{1}{4} \\
\lvert -1/2 \rvert^2 =& \frac{1}{4}
\end{align}
$

There is still a $25\%$ probability of each come. This negative sign does have an effect on the **phase** of the circuit. If we just measured the outcomes, we would not see this effect, but it is present in the quantum system. We can take advantage of this value to build a circuit with interesting effects.

The negative was made the **phase** of the qubits. It can effect the **control** qubit as well as the target qubit. When we use the **phase** to effect the value of the **control** bit it is called **phase kickback**.

## Reversed CX Gate

We can use phase kickback to create amazing features in our circuit. The following circuit will have an unexpected property.

![Reversed CX](kickback/kickback_04.png)

To understand this circuit, we can first set out the columns of gates using tensor products.

$
\begin{align}
I \oplus X =&
	\begin{bmatrix}
	1 & 0 \\
	0 & 1
	\end{bmatrix}
	\oplus
	\begin{bmatrix}
	0 & 1 \\
	1 & 0
	\end{bmatrix}
	\\
	=& \begin{bmatrix}
	0 & 1 & 0 & 0 \\
	1 & 0 & 0 & 0 \\
	0 & 0 & 0 & 1 \\
	0 & 0 & 1 & 0
	\end{bmatrix}
\\
H \oplus H =&
	\left(
	\frac{1}{\sqrt{2}}
	\begin{bmatrix}
	1 & 1 \\
	1 & -1 
	\end{bmatrix}
	\right)
	\oplus
	\left(
	\frac{1}{\sqrt{2}}
	\begin{bmatrix}
	1 & 1 \\
	1 & -1 
	\end{bmatrix}
	\right)
	\\
	=&
	\frac{1}{2}
	\left(
	\begin{bmatrix}
	1 & 1 \\
	1 & -1 
	\end{bmatrix}
	\oplus
	\begin{bmatrix}
	1 & 1 \\
	1 & -1 
	\end{bmatrix}
	\right)
	\\
	=&
	\frac{1}{2}
	\begin{bmatrix}
	1 & 1 &1 & 1 \\
	1 & -1 & 1 & -1 \\
	1 & 1 & -1 & -1 \\
	1 & -1  & -1 & 1 
	\end{bmatrix}
\end{align}
$

We can now walkthough how the circuit applies to an initial state vector.

We start in the zero state.

$
\begin{align}
	S =& \begin{bmatrix}
		1 \text{ (q0=0, q1=0) } \\
		0 \text{ (q0=0,q1=1)} \\
		0 \text{ (q0=1,q1=0)} \\
		0 \text{ (q0=1, q1=1)}
	\end{bmatrix}
\end{align}
$

We apply the $I \oplus X$ gate pair.

$
\begin{align}
	(I \oplus X) S =&
	\begin{bmatrix}
	0 & 1 & 0 & 0 \\
	1 & 0 & 0 & 0 \\
	0 & 0 & 0 & 1 \\
	0 & 0 & 1 & 0
	\end{bmatrix}
	\begin{bmatrix}
	1\\
	0\\
	0\\
	0
	\end{bmatrix}
	\\
	=& \begin{bmatrix}
	0+0+0+0 \\
	1+0+0+0 \\
	0+0+0+0 \\
	0+0+0+0 
	\end{bmatrix}
	\\=& \begin{bmatrix}
	0 \text{ (q0=0,q1=0)} \\
	1 \text{ (q0=0, q1=1)} \\
	0 \text{ (q0=1,q1=0)} \\
	0 \text{ (q0=1,q1=1)}
	\end{bmatrix}
\end{align}
$

This flips the $q_1$ bits from 0 to 1 as expected. Next, we apply two H gates.

$
\begin{align}
(H \oplus H)S =&
	\frac{1}{2}
	\begin{bmatrix}
	1 & 1 &1 & 1 \\
	1 & -1 & 1 & -1 \\
	1 & 1 & -1 & -1 \\
	1 & -1  & -1 & 1 
	\end{bmatrix}
	\begin{bmatrix}
	0 \text{ (q0=0,q1=0)} \\
	1 \text{ (q0=0, q1=1)} \\
	0 \text{ (q0=1,q1=0)} \\
	0 \text{ (q0=1,q1=1)}
	\end{bmatrix}
	\\
	=& 
	\frac{1}{2}
	\begin{bmatrix}
	1 \text{ (q0=0,q1=0)} \\
	-1 \text{ (q0=0, q1=1)} \\
	1 \text{ (q0=1,q1=0)} \\
	-1 \text{ (q0=1,q1=1)}
	\end{bmatrix}
\end{align}
$

We have introduced two negative numbers, but they do not effect the outcome yet.

- $(q_0=0, q_1=0)$ has probability $| \frac{1}{2} |^2=\frac{1}{4}=25\%$
- $(q_0=0, q_1=1)$ has probability $| \frac{-1}{2} |^2=\frac{1}{4}=25\%$
- $(q_0=1, q_1=0)$ has probability $|\frac{1}{2}|^2 =\frac{1}{4}=25\%$
- $(q_0=1, q_1=1)$ has probability $|\frac{-1}{2}|^2 =\frac{1}{4} =25\%$

Next, we apply the CX Gate.

$
\begin{align}
(CX)S =&
	\begin{bmatrix}
	1 & 0 & 0 & 0 \\
	0 & 1 & 0 & 0 \\
	0 & 0 & 0 & 1 \\
	0 & 0 & 1 & 0
	\end{bmatrix}
	\left(
	\frac{1}{2}
	\begin{bmatrix}
	1 \text{ (q0=0,q1=0)} \\
	-1 \text{ (q0=0, q1=1)} \\
	1 \text{ (q0=1,q1=0)} \\
	-1 \text{ (q0=1,q1=1)}
	\end{bmatrix}
	\right)
	\\
	=&
	\frac{1}{2}
	\begin{bmatrix}
	1  \text{ (q0=0,q1=0)} \\
	-1  \text{ (q0=0, q1=1)} \\
	-1 \text{ (q0=1,q1=0)} \\
	1  \text{ (q0=1,q1=1)}
	\end{bmatrix}
\end{align}
$

We have moved the location of a negative one, but the outcomes are still unchanged. We have changed the **phase** of two outcomes. This will effect the next step. Once we apply the last pair of H gates, something magical happens.

$
\begin{align}
(H \oplus H) S =&
	\frac{1}{2}
	\begin{bmatrix}
	1 & 1 &1 & 1 \\
	1 & -1 & 1 & -1 \\
	1 & 1 & -1 & -1 \\
	1 & -1  & -1 & 1 
	\end{bmatrix}
	\left(
	\frac{1}{2}
	\begin{bmatrix}
	1  \text{ (q0=0,q1=0)} \\
	-1  \text{ (q0=0, q1=1)} \\
	-1 \text{ (q0=1,q1=0)} \\
	1  \text{ (q0=1,q1=1)}
	\end{bmatrix}
	\right)
	\\
	=&
	\frac{1}{4}
	\left(
	\begin{bmatrix}
	1 & 1 &1 & 1 \\
	1 & -1 & 1 & -1 \\
	1 & 1 & -1 & -1 \\
	1 & -1  & -1 & 1 
	\end{bmatrix}
	\begin{bmatrix}
	1  \text{ (q0=0,q1=0)} \\
	-1  \text{ (q0=0, q1=1)} \\
	-1 \text{ (q0=1,q1=0)} \\
	1  \text{ (q0=1,q1=1)}
	\end{bmatrix}
	\right)
	\\
	=&
	\frac{1}{4}
	\begin{bmatrix}
	1*1+(-1)*1+(-1)*1+1*1 \\
	1*1+(-1)*(-1)+(-1)*1+1*(-1) \\
	1*1+(-1)*1+(-1)*(-1)+1*(-1) \\
	1*1+(-1)*(-1)+(-1)*(-1)+(1)*1
	\end{bmatrix}
	\\
	=&
	\frac{1}{4}
	\begin{bmatrix}
	1-1-1+1=0 \\
	1+1-1-1=0 \\
	1-1+1-1=0 \\
	1+1+1+1=4
	\end{bmatrix}
	\\
	=&\begin{bmatrix}
	0 \text{ (q0=0,q1=0)} \\
	0 \text{ (q0=0,q1=1)} \\
	0 \text{ (q0=1,q1=0)} \\
	1 \text{ (q0=1,q1=1)}
	\end{bmatrix}
\end{align}
$

All but one column cancelled out! We are left with only one possible outcome. There is a $100\%$ chance of getting $q_0=1$ and $q_1=1$. The application of the X gate to the control bit caused **phase kickback** to the **control** qubit. This makes the circuit deterministic. There is only one possible outcome.

We built a reversed CX gates! If the target bit is set to 1, it causes the control bit to flip!

What is really important here is what happens to the phases before and after the CX gate. We can see what is going on using the Q-Sphere.

Here is the state of the system before the CX gate is applied.

![Kickback before Circuit](kickback/phase_turn_01_circuit.png)

![Kickback before CX Gate Q-Sphere](kickback/phase_turn_01.png)

Next is the state of the system after the gate is applied.

![Kickback after Circuit](kickback/phase_turn_02_circuit.png)

![Kickback after CX Gate Q-Sphere](kickback/phase_turn_02.png)

The phase of $\left| 00 \right\rangle$ and $\left| 01 \right\rangle$ are unchanged. The phase of $\left| 10 \right\rangle$ and $\left| 11 \right\rangle$ are swapped. This corresponds to our state vectors.

The state vector before the CX gate is

$
\begin{align}
	\frac{1}{2}
	\begin{bmatrix}
	1 \text{ (q0=0,q1=0)} \\
	-1 \text{ (q0=0, q1=1)} \\
	1 \text{ (q0=1,q1=0)} \\
	-1 \text{ (q0=1,q1=1)}
	\end{bmatrix}
\end{align}
$

The state vector after the CX gate is

$
\begin{align}
	\frac{1}{2}
	\begin{bmatrix}
	1  \text{ (q0=0,q1=0)} \\
	-1  \text{ (q0=0, q1=1)} \\
	-1 \text{ (q0=1,q1=0)} \\
	1  \text{ (q0=1,q1=1)}
	\end{bmatrix}
\end{align}
$

The negative signs were exchanged on $(q0=1,q1=0)$ and $(q0=1,q1=1)$. The phase has changed in a way that effects the control bit.

## Reversed CX Gate Matrix

We only looked at what happened when $q_1$ was equal to $1$. To see if this actually makes the reversed CX gate we can look at only the H and CX gates in the circuit.

![Final Reversed CX](kickback/kickback_05.png)

This subsection of the circuit applies $(H \oplus H)$ followed by CX. Note that we multiply in the order the gates are applied.

$
\begin{align}
CX \cdot (H \oplus H) =&
\left(
	\begin{bmatrix}
	1 & 0 & 0 & 0 \\
	0 & 1 & 0 & 0 \\
	0 & 0 & 0 & 1 \\
	0 & 0 & 1 & 0
	\end{bmatrix}
\right)
\cdot
\left(
    \frac{1}{2}
	\begin{bmatrix}
	1 & 1 &1 & 1 \\
	1 & -1 & 1 & -1 \\
	1 & 1 & -1 & -1 \\
	1 & -1  & -1 & 1 
	\end{bmatrix}
\right)
\\
=&
\frac{1}{2}
\begin{bmatrix}
    1 & 1 & 1 & 1 \\
    1 & -1 & 1 & -1 \\
    1 & -1 & -1 & 1 \\
    1 & 1 & -1 & -1 
\end{bmatrix}
\end{align}
$

The last set of gates is another pair of H gates.

$
\begin{align}
(H \oplus H) \cdot (CX \cdot (H \oplus H))
=&
\left(
    \frac{1}{2}
	\begin{bmatrix}
	1 & 1 &1 & 1 \\
	1 & -1 & 1 & -1 \\
	1 & 1 & -1 & -1 \\
	1 & -1  & -1 & 1 
	\end{bmatrix} 
\right)
\cdot
\left(
\frac{1}{2}
\begin{bmatrix}
    1 & 1 & 1 & 1 \\
    1 & -1 & 1 & -1 \\
    1 & -1 & -1 & 1 \\
    1 & 1 & -1 & -1 
\end{bmatrix}
\right)
\\
=& \frac{1}{4}
\begin{bmatrix}
    4 & 0 & 0 & 0 \\
    0 & 0 & 0 & 4 \\
    0 & 0 & 4 & 0 \\
    0 & 4 & 0 & 0 
\end{bmatrix}
\\
=& 
\begin{bmatrix}
    1 & 0 & 0 & 0 \\
    0 & 0 & 0 & 1 \\
    0 & 0 & 1 & 0 \\
    0 & 1 & 0 & 0 
\end{bmatrix}
\end{align}
$

What happens when we apply this to an arbitary state?

$
\begin{align}
(H \oplus H) \cdot (CX \cdot (H \oplus H)) S=&
\begin{bmatrix}
    1 & 0 & 0 & 0 \\
    0 & 0 & 0 & 1 \\
    0 & 0 & 1 & 0 \\
    0 & 1 & 0 & 0 
\end{bmatrix}
\begin{bmatrix}
    \alpha_{00} \\
    \alpha_{01} \\
    \alpha_{10} \\
    \alpha_{11}
\end{bmatrix}
\\
=&
\begin{bmatrix}
    \alpha_{00} \text{ (q0=0, q1=0)} \\
    \alpha_{11} \text{ (q0=0, q1=1)}\\
    \alpha_{10} \text{ (q0=1, q1=0)}\\
    \alpha_{01} \text{ (q0=1, q1=1)}\\
\end{bmatrix}
\end{align}
$

The first position in our state vector says that the original value of $\dirac{00}$ same value it was initially. The second position states that the value of $\dirac{11}$ and $\dirac{10}$ have swapped. When $q_1=1$ the value of $q_0$ swapped from 1 to 0. The third position states that $\dirac{10}$ is unchanged because $q_1$ was set to 0. The final position states that $\dirac{10}$ and $\dirac{11}$ have swapped positions. In this situations, $q_1=1$ caused $q_0$ to swap from 1 to 0.

We have build a reversed CX gate using phase kickback.
