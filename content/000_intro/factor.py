#Mark Boady - 2021

#Import the time library
import time

#findFactor either finds a prime
#factor or returns the number if
#it is prime
def findFactor(num):
    k=2
    while k*k <= num:
        if num%k==0:
            return k
        k=k+1
    #Number is Prime
    return num
#factor takes a number and
#repeatedly finds factors
#until all are found
def factor(num):
    factors=[]
    while num > 1:
        k = findFactor(num)
        num=num//k
        factors.append(k)
    return factors

#Main function runs tests
def main():
    #Example Numbers to Factor
    values=[15,21,35,143, 59989, 376289]
    for target in values:
        before=time.time()
        primeFactors = factor(target)
        after=time.time()
        print("The Factors of {:d} are".format(target))
        print(primeFactors)
        print("Finding them took {:0.5f} seconds".format(after-before))
#Run the Script
if __name__=="__main__":
    main()