from qiskit import *
from qiskit import BasicAer
import sys
import os.path

q = QuantumRegister(3,'q')
c = ClassicalRegister(3,'c')
circ = QuantumCircuit(q,c)

#Build a Circuit
circ.h(q[0])
circ.h(q[1])
circ.barrier(q[0],q[1],q[2])

circ.x(q[0])
circ.x(q[1])
circ.x(q[2])
circ.ccx(q[0],q[1],q[2])
circ.x(q[0])
circ.x(q[1])

circ.barrier(q[0],q[1],q[2])
circ.measure(q[0],c[0])
circ.measure(q[1],c[1])
circ.measure(q[2],c[2])

#X=circ.draw(output="latex_source")
#print(X)

#Matplot to make an image
fn = sys.argv[0]
fn = os.path.splitext(fn)[0]
fn = fn+".png"
circ.draw(output="mpl",filename=fn)

#Run an experiment!
backend_sim = BasicAer.get_backend('qasm_simulator')
job_sim = execute(circ,backend_sim,shots=2048)
result_sim = job_sim.result()
counts = result_sim.get_counts()
print(counts)
