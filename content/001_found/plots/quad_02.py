import matplotlib.pyplot as plt
import numpy as np
import sys
import os.path

#Set up the problem
xRangeStart=-10
xRangeStop=10
xStep=0.20
f = lambda x: x**2+2*x+3

#Make the Points
xPoints=[]
yPoints=[]
y2Points=[]

x=xRangeStart
while x <= xRangeStop:
    y=f(x)
    xPoints.append(x)
    yPoints.append(y)
    y2Points.append(0)
    x+=xStep

#Plot the points
plt.plot(xPoints,yPoints)
plt.plot(xPoints,y2Points)
plt.legend(["h(x)=x^2+2x+3","g(x)=0"])
plt.title("Solutions to h(x)=g(x)")
#Show for testing
#plt.show()
#Save as Image
filename = sys.argv[0]
filename = os.path.splitext(filename)[0]
targetName=filename+".png"
plt.savefig(targetName)
