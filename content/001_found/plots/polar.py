import matplotlib.pyplot as plt
import numpy as np
import sys
import os.path


plt.axes(projection='polar')
plt.title('Location of 9+5i')
plt.polar(0.50798,56.62155,'o')

#Save as Image
filename = sys.argv[0]
filename = os.path.splitext(filename)[0]
targetName=filename+".png"
plt.savefig(targetName)
