# Qubits


```{math}
\newcommand{\qzero}[0]{\left|0\right\rangle}
\newcommand{\qone}[0]{\left|1\right\rangle}
\newcommand{\qplus}[0]{\left| + \right\rangle}
\newcommand{\qminus}[0]{\left| - \right\rangle}
\newcommand{\dirac}[1]{\left| #1 \right\rangle}
\newcommand{\ident}[0]{\mathrm{I}}
```
## Classic Bits

With a classical computer the bit is determined by voltage on a wire.

- 0 (Low Voltage 0-2 Volts)
- 1 (High Voltage 3-5 Volts)

On a classic computer, bits and circuits are governed by classical physics.

<a title="El pak at English Wikipedia, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Original_message.jpg"><img width="256" alt="Original message" src="https://upload.wikimedia.org/wikipedia/commons/c/cf/Original_message.jpg"></a>

The foundation of all classical computers is the application of logic gates to bits. Gates work on the bits using traditional logic. The circuit for a classical full adder is shown below.

<a title="Inductiveload, Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Full-adder_logic_diagram.svg"><img width="256" alt="Full-adder logic diagram" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/Full-adder_logic_diagram.svg/256px-Full-adder_logic_diagram.svg.png"></a>

## Quantum Bits

Quantum Computers are built on a different foundation. The **Qubit** is the quantum equivalent of a bit. Gates are governed by Linear Algebra not Boolean Logic. Qubits are governed by Quantum Physics not Classical Physics. Writing Quantum Algorithms requires a completely different perspective than classical algorithms.

When **measured** a qubit can have one of two values
- $\qzero$ the quantum **False** or 0 state
- $\qone$ the quantum **True** or 1 state

This way of writing values is called **Dirac Notation**. During computation a qubit can be in **superposition**. A **superposition** is a linear combination of states.

$
\begin{align}
	\dirac{\psi} = \alpha \qzero + \beta \qone
\end{align}
$

This means that a qubit is a linear combination of states.

$
\begin{align}
	\dirac{\psi} = \alpha \qzero + \beta \qone
\end{align}
$

The values of $\alpha$ and $\beta$ are **complex numbers**.  When the qubit is measured it will be either 0 or 1. The probability of a 0 is $\left|\alpha\right|^2$. The probability of a 1 is $\left| \beta \right|^2$. 

Since both are probabilities they must add together to get 1. There is a 100% chance the qubit is either 0 or 1.

$
\begin{align*}
	\left|\alpha\right|^2 + \left| \beta \right|^2 = 1
\end{align*}
$

A qubit has a *continuum* of states between $\qzero$ and $\qone$. When **measured** the qubit will either be in state $0$ or $1$, but until we measure it the qubit can be in any pobability.

A qubit with a 50/50 split between $0$ and $1$ is shown below.

$
\begin{align}
	\qplus =& \frac{1}{\sqrt{2}} \qzero + \frac{1}{\sqrt{2}} \qone \\
	\left|\alpha\right|^2 =& \left| \frac{1}{\sqrt{2}} \right|^2 = \frac{1}{2} = 50\% \\
	\left|\beta\right|^2 =& \left| \frac{1}{\sqrt{2}} \right|^2 = \frac{1}{2} = 50\%\\
\end{align} 
$

In a classical computer, we talk about bits but they are really electric singals. What is a qubit in reality?

There are many quantum particles we can use a qubits and different features we can meaure of each.

- two polarizations of a photon
- alignment of the nuclear spin in a uniform magnetic field
- two states of an electron orbiting a single atom

For our purposes: It doesn't matter. We will leave that to physics. Just like how we can design a circle using just logic, if we understand the math behind the qubits we can design circuits without understanding all the physics.

One way to imagine a qubit is a **Bloch Sphere**. We imagine the qubit is a sphere. Imagine there is a dot painted on the qubit. When we measure the qubit, we will find out where the dot is. A dot at the top of the sphere means a 100% chance of measuring 0. A dot at the bottom of the sphere has a 100% chance of measuring 1. If you put the dot at some point in the middle, you might get a 1 and might get a 0. The probability of each depends on where you places the dot.

<a title="Smite-Meister, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Bloch_sphere.svg"><img width="128" alt="Bloch sphere" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Bloch_sphere.svg/128px-Bloch_sphere.svg.png"></a>

To determine our point on the sphere, we used the fact that $\left|\alpha\right|^2 + \left| \beta \right|^2 = 1$. We can find the point on the sphere.

$
\begin{align}
	\dirac{\psi} =& \alpha \qzero + \beta \qone \\
	\dirac{\psi} =& e^{i \gamma} \left( \cos \frac{\theta}{2} \qzero + e^{i \varphi} \sin \frac{\theta}{2} \qone \right)
\end{align}
$

The factor $e^{i \gamma}$ has no *observable effects* and is normally ignored in algorithms.

## Qubit Information

How much information is in a qubit? The sphere has an infinite number of points Measuring a single qubit only gives 0 or 1. Running multiple tests lets us approximate $\alpha$ and $\beta$. If we measure $\qplus$ and get 0, then the qubit will measure $\qzero$ in any later test. No one knows why this happens!

A qubit can be drawn as a vector.

$
\begin{align}
	\dirac{\psi} = \alpha \qzero + \beta \qone = \begin{bmatrix} \alpha \\ \beta \end{bmatrix}
\end{align}
$

A multiple qubit system is a larger vector. A two-qubit system has a probability for each result.

$
\begin{align}
	\dirac{\psi} =& \alpha_{00} \dirac{00} + \alpha_{01} \dirac{01} + \alpha_{10} \dirac{10} + \alpha_{11} \dirac{11}
		\\
		=& \begin{bmatrix} \alpha_{00} \\ \alpha_{01} \\ \alpha_{10} \\ \alpha_{11} \end{bmatrix}
\end{align}
$

A three qubit system would be a vector of $2^3=8$ complex numbers. There are 8 possible settings of 3 bits and each one has a complex number. The system could have an even split between all 8 different outcomes. 

More generally, an $n$ qubit system is a vector of $2^n$ complex numbers. This makes simulating quantum systems very difficult. Imagine you want to simulate a quantum computer on a classical computer. To store a complex number, you need to store 2 floats. Let's say you use 64-bit floats. Each complex number takes $64*2$ bits. If you have an $n$ qubit system, you need $(2^n)(64*2)$ bits. To simulate a 6 qubit system on a classic computer we need 8192 bits just to store the vector. To store the state vector of a 32 qubit system would require 549755813888 bits which is 68.7 GB!

