#include "complex.hpp"

Complex::Complex(long double r, long double i){
    this->re = r;
    this->img = i;
}
Complex::Complex(const Complex &C){
    this->re = C.real();
    this->img = C.imaginary();
}
Complex::~Complex(){
    return;
}
long double Complex::real() const{
    return this->re;
}
long double Complex::imaginary() const{
    return this->img;
}
friend ostream& operator<<(ostream& os, const Complex& num){
    if( std::abs(num.imaginary()) < 0.0001 ){
        os << std::setprecision(5) << num.real();
    }
    os << std::setprecision(5)
        << num.real()
        << "+"
        << num.imaginary()
        << "i";
    return os;
}
