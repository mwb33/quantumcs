/**
    @file
    @author Mark Boady <mwb33@drexel.edu>
    @date 2023
    @section Description
 
    A complex number class for simulating Quantum Algorithms.
 */
/**
 Representation of a Complex Number.
 */
class Complex{
private:
    long double re;/**< The real part of the number.*/
    long double img;/**<The imaginary part of the number.*/
public:
    /**
     Create a new Complex Number
     @param r is the real part of the number
     @param i is the imaginary part of the number.
     */
    Complex(long double r=0, long double i=0);
    /**
        Copy a Complex Number
        @param C is the complex number to copy
     */
    Complex(const Complex &C);
    /**
        Destroy the Complex Number
     */
    ~Complex();
    /**
        Get the real part of the number
        @return the real part of the number
     */
    long double real() const;
    /**
        Get the imaginary part of the number
        @return the imaginary part of the number
     */
    long double imaginary() const;
    /**
     Print the Complex Number to out.
     @param os is the stream to print to
     @param num is the complex number to print
     @return the modified stream
     */
    friend ostream& operator<<(ostream& os, const Complex& num);
};
