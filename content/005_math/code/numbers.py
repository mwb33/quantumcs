#Convert a number from base-10 to given base
def toBase(num,base):
    N=[]#place to store digits
    while num > 0:
        rem = num % base #remainder in python
        num = num // base #quotient in python
        N.append(rem)
    #Digits are backward, reverse
    N=N[::-1]
    #Return array of digits
    return N

#Convert from base given back to base-10
def fromBase(num,base):
    result=0#Final Answer
    n = len(num)
    #Summation
    for i in range(0,n):
        #L_{i} * b^{n-1-i}
        result=result+num[i]*(base**(n-1-i))
    #return final answer
    return result

num=28
print(toBase(num,2))
print(toBase(num,3))
print(toBase(num,5))

A=toBase(num,2)
B=toBase(num,3)
C=toBase(num,5)
print(fromBase(A,2))
print(fromBase(B,3))
print(fromBase(C,5))

print(toBase(900,256))
print(toBase(350,256))
print(toBase(900*350,256))