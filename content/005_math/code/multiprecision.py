#A multiprecision number is just
#a base and list of digits

#A Test class for large ints
#Store the digits in reverse order such that
#Value = sum( base^index * digits[index] , index in digits)
class BigInt:
    #Create an empty array for digits
    #and save the base
    def __init__(self,digits,base):
        self.base = base
        self.digits = digits
    def __str__(self):
        res="Digits: "+str(self.digits)+"\n"
        res+="Base:"+str(self.base)+"\n"
        res+="Real Value:"+str(fromBase(self))+"\n"
        return res
#Classical Addition
def add(A,B):
    #Sanity Check
    if A.base != B.base:
        raise Exception("Bases do no match!")
    aDigits = A.digits
    bDigits = B.digits
    cDigits = [] #For result
    #We start with no carry
    carry = 0
    #Max Number of digits
    numDigits = max(len(aDigits),len(bDigits))
    #Loop from back to front
    for i in range(0,numDigits):
        a = aDigits[i] if i < len(aDigits) else 0
        b = bDigits[i] if i < len(bDigits) else 0
        newValue = a+b+carry
        newDigit = newValue%A.base
        carry = newValue//A.base
        cDigits.append(newDigit)
    if carry > 0:
        cDigits.append(carry)
    return BigInt(cDigits,A.base)
    
#Classical Multiply
def mult(A,B):
    #Sanity Check
    if A.base != B.base:
        raise Exception("Bases do no match!")
    #Get Details of Numbers
    aDigits=A.digits
    aSize = len(aDigits)
    bDigits=B.digits
    bSize = len(bDigits)
    #Place to store result
    #Zero fill
    cDigits=[0]*(aSize+bSize)
    #We might carry during addition step
    carry = 0
    #Loop over B first
    for i in range(0,bSize):
        for j in range(0,aSize):
            #Get Digits
            a = aDigits[j] if j < aSize else 0
            b = bDigits[i] if i < bSize else 0
            #Compute this location
            cDigits[i+j] = cDigits[i+j] + b*a + carry
            #Carry overflow to next column
            carry = cDigits[i+j]//A.base
            cDigits[i+j]=cDigits[i+j]%A.base
        cDigits[i+aSize]=carry
        carry=0
    return BigInt(cDigits,A.base)

#Karatuba
def karatsuba(A,B):
    #Sanity Check
    if A.base != B.base:
        raise Expection("Cannot Karatsuba Different Bases")
    #Base Case
    if (len(A.digits) < 2 or len(B.digits) < 2):
        return mult(A,B)
    #Size of the numbers
    m = min(len(A.digits),len(B.digits))
    middle = m//2
    #Split each into high and low
    aDigits = A.digits
    bDigits = B.digits
    aLow = BigInt(aDigits[0:middle],A.base)
    aHigh = BigInt(aDigits[middle:],A.base)
    bLow = BigInt(bDigits[0:middle],A.base)
    bHigh = BigInt(bDigits[middle:],A.base)
    #Compute Parts
    low = karatsuba(aLow,bLow)
    middle = karatsuba(add(aLow,aHigh),add(bLow,bHigh))
    high = karatsuba(aHigh,bHigh)
    #Put Back Together
    result = 
    return BigInt([],A.base)

#Since Python already handles multiprecision
#We can test with large numbers easily!
#Convert a number from base-10 to given base
def toBase(num,base):
    N=[]#place to store digits
    while num > 0:
        rem = num % base #remainder in python
        num = num // base #quotient in python
        N.append(rem)
    #Return array of digits
    return BigInt(N,base)

#Convert from base given back to base-10
def fromBase(Number):
    #Break up the Data Structure
    num = Number.digits
    base = Number.base
    #Convert Back to Python
    result=0#Final Answer
    n = len(num)
    #Summation
    for i in range(0,n):
        #L_{i} * b^{n-1-i}
        result=result+num[i]*(base**(i))
    #return final answer
    return result

if __name__=="__main__":
    x = 564351
    y = 35415645
    base = 10
    X = toBase(x,base)
    Y = toBase(y,base)
    
    print("X Object",)
    print(X)
    print("Y Object")
    print(Y)
    
    Z = add(X,Y)
    print("Expected Value:",x+y)
    print("Z Object")
    print(Z)
    
    M = mult(X,Y)
    print("Expected Value:",x*y)
    print("M Object")
    print(M)
    
    N = karatsuba(X,Y)
    print("Expected Value:",x*y)
    print("N Object")
    print(N)
