#Implementation of
#Quantum Ciruict using if
#statements to better
#understand CCX and CX
#in this application
def adder(a,b,carryIn,carryOut=False):
    #Determine if a+b causes
    #carry of 1
    if a and b:
        carryOut=not carryOut
    #Is a XOR b true
    #This means a+b=1
    if a:
        b = not b
    #If a+b==1 and carryIn==1
    #The carryOut is wrong
    #1+1=10!
    if b and carryIn:
        carryOut = not carryOut
    #if a+b!=1 then
    #result = carryIn+0
    #if a+b==1 then
    #result = carryIn+1
    if b:
        carryIn = not carryIn
    #Undo change to b
    #to return in original state
    if a:
        b=not b
    return (a,b,carryOut,carryIn)

#Generate all Possible Inputs
def options():
    values=[]
    for i in range(0,8):
        temp=[False,False,False]
        temp[2]=bool(i%2)
        temp[1]=bool((i//2)%2)
        temp[0]=bool((i//2//2)%2)
        values.append(temp)
    return values

#Prints as pretty binary
def binary(v,x):
    a=int(v[0])
    b=int(v[1])
    c=int(v[2])
    m=int(x[2])
    n=int(x[3])
    fmt="{:d}+{:d}+{:d}={:d}{:d}"
    return fmt.format(a,b,c,m,n)

#Print all options as an example
if __name__=="__main__":
    count=0
    values = options()
    for v in values:
        print("Case",count)
        print("A=",v[0])
        print("B=",v[1])
        print("Carry In=",v[2])
        print("Result:")
        x=adder(v[0],v[1],v[2])
        print("A=",x[0])
        print("B=",x[1])
        print("Carry Out=",x[2])
        print("Result=",x[3])
        print(x)
        print(binary(v,x))
        print()
        count+=1
