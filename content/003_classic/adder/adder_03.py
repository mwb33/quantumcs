from qiskit import *
from qiskit import BasicAer

qc = QuantumCircuit(4, 4)
#Wires:
#q0 - input 1
#q1 - input 2
#q2 - result
#q3 - carry out
qc.h(0)
qc.h(1)
qc.h(2)
qc.barrier(range(0,4))
qc.ccx(0,1,3)
qc.cx(0,1)
qc.ccx(1,2,3)

qc.barrier(range(0,4))
qc.cx(0,1)
qc.barrier(range(0,4))
qc.measure(0,0)
qc.measure(1,1)
qc.measure(2,2)
qc.measure(3,3)

#Run our simulation!
#Create Simulator
backend_sim = BasicAer.get_backend('qasm_simulator')
#Run 2,048 tests
job_sim = execute(qc,backend_sim,shots=2048)
#Get the results
result_sim = job_sim.result()
#Show the count of each outcome
counts = result_sim.get_counts()
print(counts)

qc.draw(output="mpl",filename="adder_03.png")
print(qc.draw(output="text"))
