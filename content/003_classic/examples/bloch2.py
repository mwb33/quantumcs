#Mark Boady - 2023
#Bloch Sphere example added

#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer
#For the Bloch Sphere
from qiskit.visualization import plot_bloch_multivector


#We need a circuit with
#3 Qubits and 3 classic bits
qc = QuantumCircuit(3,3)

#Add an X gate
qc.x(0)

#We need to measure to see the results
#Barrier is just for visual
qc.barrier(range(0,3))
qc.measure(0,0)
qc.measure(1,1)
qc.measure(2,2)

#Print as text
print(qc.draw(output="text"))

#Use a State Vector Back end
backend = BasicAer.get_backend('statevector_simulator')
job=execute(qc,backend)
results = job.result()
#Get the state vector to 3 decminal places
SV=results.get_statevector(qc,decimals=3)
print(SV)

#Use a State Vector Back end
backend = BasicAer.get_backend('statevector_simulator')
job=execute(qc,backend)
results = job.result()
#Get the state vector to 3 decminal places
SV=results.get_statevector(qc,decimals=3)

fig = plot_bloch_multivector(SV)
fig.savefig("bloch2.png")
