#Mark Boady - 2023
#Logic Gates - XOR

#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer
#For the Bloch Sphere
from qiskit.visualization import plot_bloch_multivector

#Make a trivial Bloch Sphere

qc = QuantumCircuit(1,1)
qc.h(0)

#Use a State Vector Back end
backend = BasicAer.get_backend('statevector_simulator')
job=execute(qc,backend)
results = job.result()
#Get the state vector to 3 decminal places
SV=results.get_statevector(qc,decimals=3)

fig = plot_bloch_multivector(SV)
fig.savefig("bloch1.png")
