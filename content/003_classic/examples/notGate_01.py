from qiskit import *
from qiskit import BasicAer

qc = QuantumCircuit(1, 1)

qc.x(0)
qc.measure(0,0)

#Run our simulation!
#Create Simulator
backend_sim = BasicAer.get_backend('qasm_simulator')
#Run 2,048 tests
job_sim = execute(qc,backend_sim,shots=2048)
#Get the results
result_sim = job_sim.result()
#Show the count of each outcome
counts = result_sim.get_counts()
print(counts)

qc.draw(output="mpl",filename="notGate.png")
