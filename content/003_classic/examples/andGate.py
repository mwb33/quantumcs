#Mark Boady - 2021
#Logic Gates - AND

#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer

#Make a Circuit
qc = QuantumCircuit(3,3)
#Add a CCX Gate
qc.h(0)
qc.h(1)
qc.ccx(0,1,2)
#Measure out results
qc.barrier(range(0,3))
qc.measure(0,0)
qc.measure(1,1)
qc.measure(2,2)

#Matplot to make an image
qc.draw(output="mpl",filename="andGate.png")

#Run our simulation!
#Create Simulator
backend_sim = BasicAer.get_backend('qasm_simulator')
#Run 2,048 tests
job_sim = execute(qc,backend_sim,shots=2048)
#Get the results
result_sim = job_sim.result()
#Show the count of each outcome
counts = result_sim.get_counts()
print(counts)

