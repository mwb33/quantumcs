from qiskit import *
from qiskit import BasicAer

qc = QuantumCircuit(2, 2)
qc.h(0)
qc.x(1)
qc.cx(0,1)
qc.measure(0,0)
qc.measure(1,1)

#Run our simulation!
#Create Simulator
backend_sim = BasicAer.get_backend('qasm_simulator')
#Run 2,048 tests
job_sim = execute(qc,backend_sim,shots=2048)
#Get the results
result_sim = job_sim.result()
#Show the count of each outcome
counts = result_sim.get_counts()
print(counts)

qc.draw(output="mpl",filename="notGate2.png")
print(qc.draw(output="text"))
