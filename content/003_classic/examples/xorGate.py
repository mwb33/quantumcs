#Mark Boady - 2021
#Logic Gates - XOR

#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer

#Make a Circuit
qc = QuantumCircuit(5,3)
#Add a CCX Gate
qc.h(0)
qc.h(1)
#q_3 is for -(A and B)
qc.ccx(0,1,3)
qc.x(3)
#q4 is for -(-A wedge -B)
qc.x(0)
qc.x(1)
qc.ccx(0,1,4)
qc.x(4)#Not the result
qc.x(0)#undo action
qc.x(1)#undo action
#q2 is for q3 and q4
qc.ccx(3,4,2)
#Measure out results
qc.barrier(range(0,5))
qc.measure(0,0)
qc.measure(1,1)
qc.measure(2,2)

#Matplot to make an image
qc.draw(output="mpl",filename="xorGate.png")

#Run our simulation!
#Create Simulator
backend_sim = BasicAer.get_backend('qasm_simulator')
#Run 2,048 tests
job_sim = execute(qc,backend_sim,shots=2048)
#Get the results
result_sim = job_sim.result()
#Show the count of each outcome
counts = result_sim.get_counts()
print(counts)

