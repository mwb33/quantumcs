#Mark Boady - 2023
#Spheres for phase kickback
import os.path
import sys
#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer
#For the Bloch Sphere
from qiskit.visualization import plot_bloch_multivector

qc = QuantumCircuit(2,2)
qc.h(0)

#qc.barrier(range(0,2))
#qc.measure(0,0)
#qc.measure(1,1)

#Print as text
print(qc.draw(output="text"))

#Use a State Vector Back end to get a sphere
backend = BasicAer.get_backend('statevector_simulator')
job=execute(qc,backend)
results = job.result()
#Get the state vector to 3 decminal places
SV=results.get_statevector(qc,decimals=3)

fig = plot_bloch_multivector(SV)
#Use my name!
filename = sys.argv[0]
rootName = os.path.splitext(filename)[0]
filename = rootName+".png"
print("Save to %s"%(filename))
fig.savefig(filename)
