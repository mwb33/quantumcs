#Mark Boady - 2023
#Spheres for phase kickback
import os.path
import sys
#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer
#For the Bloch Sphere
from qiskit.visualization import plot_state_qsphere

qc = QuantumCircuit(2,2)
qc.h(0)
qc.x(1)
qc.h(1)
qc.cx(0,1)

#qc.barrier(range(0,2))
#qc.measure(0,0)
#qc.measure(1,1)

#Print as text


#Use a State Vector Back end to get a sphere
backend = BasicAer.get_backend('statevector_simulator')
job=execute(qc,backend)
results = job.result()
#Get the state vector to 3 decminal places
SV=results.get_statevector(qc,decimals=3)
print(SV)

fig = plot_state_qsphere(SV)
#Use my name!
filename = sys.argv[0]
rootName = os.path.splitext(filename)[0]
filename = rootName+".png"
filename2 = rootName+"_circuit.png"
print("Save to %s"%(filename))
fig.savefig(filename)

print(qc.draw(output="text"))
qc.draw(output="mpl",filename=filename2)
