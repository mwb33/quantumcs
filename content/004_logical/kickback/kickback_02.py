from qiskit import *
from qiskit import BasicAer
import sys
import os.path

qc = QuantumCircuit(2, 2)
qc.h(0)
qc.h(1)
qc.cx(0,1)

qc.barrier(range(0,2))
qc.measure(0,0)
qc.measure(1,1)

#Run our simulation!
#Create Simulator
backend_sim = BasicAer.get_backend('qasm_simulator')
#Run 2,048 tests
job_sim = execute(qc,backend_sim,shots=2048)
#Get the results
result_sim = job_sim.result()
#Show the count of each outcome
counts = result_sim.get_counts()
print(counts)

#Determine File Name
myName = sys.argv[0]
baseName = os.path.splitext(myName)[0]
newName = baseName+".png"

qc.draw(output="mpl",filename=newName)
print(qc.draw(output="text"))
