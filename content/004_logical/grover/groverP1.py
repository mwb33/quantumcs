#Mark Boady - 2021

#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer

#Make a Circuit
qc = QuantumCircuit(5,4)

#Prepare for Grover
#1.) Apply X to result
qc.x(3)
#2.) Apply H to input qubits
qc.h(0)
qc.h(1)
qc.h(2)
#3.) Apply H to result qubit
qc.h(3)

#Start of Search Circuit
qc.barrier(range(0,5))
#Start of Number Selection
#Set Bits we need to be 0
qc.x(2)
#Three Qubit AND
qc.ccx(0,1,4)
qc.ccx(2,4,3)
qc.ccx(0,1,4)
#End Three QUBIT AND
#undo bit set
qc.x(2)
#End of Number Selection
qc.barrier(range(0,5))
#End of Search Circuit

#Measure out results
qc.barrier(range(0,5))
qc.measure(0,0)
qc.measure(1,1)
qc.measure(2,2)
qc.measure(3,3)

#Matplot to make an image
qc.draw(output="mpl",filename="groverP1.png")

#Run our simulation!
#Create Simulator
backend_sim = BasicAer.get_backend('qasm_simulator')
#Run 2,048 tests
job_sim = execute(qc,backend_sim,shots=2048)
#Get the results
result_sim = job_sim.result()
#Show the count of each outcome
counts = result_sim.get_counts()
print(counts)

