#Mark Boady - 2021

#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer

#Search for 3
def search(circuit):
    #Start of Search Circuit
    circuit.barrier(range(0,5))
    #Start of Number Selection
    #Set Bits we need to be 0
    circuit.x(2)
    #Three Qubit AND
    circuit.ccx(0,1,4)
    circuit.ccx(2,4,3)
    circuit.ccx(0,1,4)
    #End Three QUBIT AND
    #undo bit set
    circuit.x(2)
    #End of Number Selection
    circuit.barrier(range(0,5))
    #End of Search Circuit

#Grover Setup
def setup(circuit):
    #Prepare for Grover
    #1.) Apply X to result
    circuit.x(3)
    #2.) Apply H to input qubits
    circuit.h(0)
    circuit.h(1)
    circuit.h(2)
    #3.) Apply H to result qubit
    circuit.h(3)
    
#Diffuser
def diffuser(circuit):
    #Start of Diffuser
    circuit.h(0)
    circuit.h(1)
    circuit.h(2)

    circuit.x(0)
    circuit.x(1)
    circuit.x(2)
    #3 bit controlled Z
    circuit.ccx(0,1,4)
    circuit.cz(4,2)
    circuit.ccx(0,1,4)
    #end 3-bit controlled Z
    circuit.x(0)
    circuit.x(1)
    circuit.x(2)

    circuit.h(0)
    circuit.h(1)
    circuit.h(2)
    #End of Diffuser

#Make a Circuit
qc = QuantumCircuit(5,4)

setup(qc)
search(qc)
diffuser(qc)

#Measure out results
qc.barrier(range(0,5))
qc.measure(0,0)
qc.measure(1,1)
qc.measure(2,2)
qc.measure(3,3)

#Matplot to make an image
qc.draw(output="mpl",filename="groverP2.png")

#Run our simulation!
#Create Simulator
backend_sim = BasicAer.get_backend('qasm_simulator')
#Run 2,048 tests
job_sim = execute(qc,backend_sim,shots=2048)
#Get the results
result_sim = job_sim.result()
#Show the count of each outcome
counts = result_sim.get_counts()
print(counts)

