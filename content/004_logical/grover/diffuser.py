#Mark Boady - 2021

#Import the Libraries
from qiskit import *
#For simulations:
from qiskit import BasicAer

#Make a Circuit
qc = QuantumCircuit(5,4)

#Start of Diffuser
qc.h(0)
qc.h(1)
qc.h(2)

qc.x(0)
qc.x(1)
qc.x(2)
#3 bit controlled Z
qc.ccx(0,1,4)
qc.cz(4,2)
qc.ccx(0,1,4)
#end 3-bit controlled Z
qc.x(0)
qc.x(1)
qc.x(2)

qc.h(0)
qc.h(1)
qc.h(2)
#End of Diffuser

#Measure out results
qc.barrier(range(0,5))
qc.measure(0,0)
qc.measure(1,1)
qc.measure(2,2)
qc.measure(3,3)

#Matplot to make an image
qc.draw(output="mpl",filename="diffuser.png")

#Run our simulation!
#Create Simulator
backend_sim = BasicAer.get_backend('qasm_simulator')
#Run 2,048 tests
job_sim = execute(qc,backend_sim,shots=2048)
#Get the results
result_sim = job_sim.result()
#Show the count of each outcome
counts = result_sim.get_counts()
print(counts)

