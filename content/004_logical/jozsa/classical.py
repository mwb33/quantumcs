#Mark Boady
#Determine Balanced/Constant using a classic computer.

#Implement f(a0,a1)= a0 <-> a1
def f(x):
    a0=x[0]
    a1=x[1]
    return a0==a1

#Implement h(a0,a1)=(a0 -> a1) v (a1->a0)
def h(x):
    a0=x[0]
    a1=x[1]
    return (not a0 or a1) or (not a1 or a0)

#Convert an Integer to an n-bit array
def intToBin(num,bits):
    #Prefill all zeros
    B = [0 for x in range(0,bits)]
    #Compute Bits
    i=0 #Track Current Bit
    while num > 0:
        B[i]=num%2
        num=num//2
        i+=1
    return B
#Determine Constant or Balanced
#Returns true on constant
#and false on balanced
def isConstant(func, bits):
    start = 0
    stop = (2**(bits-1))+1
    trueCount=0
    falseCount=0
    while start <= stop:
        myInput = intToBin(start,bits)
        if func(myInput)==0:
            trueCount+=1
        else:
            falseCount+=1
        if trueCount>0 and falseCount>0:
            return False
        start+=1
    return True

if __name__=="__main__":
    print("Test Function f")
    if isConstant(f,2):
        print("Function is Constant")
    else:
        print("Function is Balanced")
    print("Test Function h")
    if isConstant(h,2):
        print("Function is Constant")
    else:
        print("Function is Balanced")
        
    