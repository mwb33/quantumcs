from qiskit import *
from qiskit import BasicAer
import sys
import os.path

qc = QuantumCircuit(3, 3)


qc.cx(0,1)
qc.cx(1,2)
qc.cx(0,1)


#Determine File Name
myName = sys.argv[0]
baseName = os.path.splitext(myName)[0]
newName = baseName+".png"

qc.draw(output="mpl",filename=newName)
print(qc.draw(output="text"))
