# Simon's Algorithm

```{math}
\newcommand{\qzero}[0]{\left|0\right\rangle}
\newcommand{\qone}[0]{\left|1\right\rangle}
\newcommand{\qplus}[0]{\left| + \right\rangle}
\newcommand{\qminus}[0]{\left| - \right\rangle}
\newcommand{\dirac}[1]{\left| #1 \right\rangle}
\newcommand{\ident}[0]{\mathrm{I}}
```

TODO: make this page

For future use:

```python
#Define an oracle for 2 bit numbers
#0,1,2,3
#This is a 2-1 mapping
def f(x):
    if x==0:
        return 1
    if x==1:
        return 1
    if x==2:
        return 2
    if x==3:
        return 2
def g(x):
    return x
def findPair(oracle):
    P=[0,1,2,3]
    n=len(P)//2+1
    D={}
    for i in range(0,n):
        a = oracle(P[i])
        if a not in D.keys():
            D[a] = P[i]
        else:
            return (D[a],P[i])
    return 0
            
answer = findPair(f)
print("The Pair of Numbers was",answer)
if answer==0:
    print("1 to 1 function")
else:
    print("2 to 1 function")
    print(answer[0] ^ answer[1])

```
